package com.devcamp.jbr0440.restapi;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CustomerInvoiceController {
    @CrossOrigin
    @GetMapping("/invoices")
    public static String main(String[] args) {
        Customer customer1 = new Customer(1, "Hung", 10000);
        Customer customer2 = new Customer(2, "Nam", 20000);
        Customer customer3 = new Customer(3, "Phuong", 30000);

        System.out.println(customer1.toString());
        System.out.println(customer2.toString());
        System.out.println(customer3.toString());

        Invoice invoice1 = new Invoice(123, customer1, 1.0);
        Invoice invoice2 = new Invoice(456, customer2, 1.0);
        Invoice invoice3 = new Invoice(789, customer3, 1.0);

        System.out.println(invoice1.toString());
        System.out.println(invoice2.toString());
        System.out.println(invoice3.toString());

        ArrayList<Invoice> listInvoice = new ArrayList<Invoice>();

        listInvoice.add(invoice1);
        listInvoice.add(invoice2);
        listInvoice.add(invoice3);

        return String.format("Danh sách hóa đơn \n %s", listInvoice);
    }
}
