package com.devcamp.jbr0460.restapi;

import java.util.ArrayList;
import java.util.Date;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CustomerVisitController {
    @CrossOrigin
    @GetMapping("/visits")
    public static String main(String[] args) {
        Customer customer1 = new Customer("Hung", true, "vip");
        Customer customer2 = new Customer("Nam", true, "medium");
        Customer customer3 = new Customer("Phuong", true, "basic");

        System.out.println(customer1.toString());
        System.out.println(customer2.toString());
        System.out.println(customer3.toString());

        Date date1 = new Date();
        Visit visit1 = new Visit(customer1, date1, 1.0, 2.0);
        Visit visit2 = new Visit(customer2, date1, 1.0, 2.0);
        Visit visit3 = new Visit(customer3, date1, 1.0, 2.0);

        System.out.println(visit1.toString());
        System.out.println(visit2.toString());
        System.out.println(visit3.toString());

        ArrayList<Visit> listVisit = new ArrayList<Visit>();

        listVisit.add(visit1);
        listVisit.add(visit2);
        listVisit.add(visit3);

        return String.format("Danh sách chuyến đi \n %s", listVisit);
    }

    
}
