package com.devcamp.jbr0460.restapi;

import java.util.Date;

public class Visit {
    Customer customer;
    Date date;
    double serviceExpensive;
    double productExpensive;
    
    public Visit() {
    }

    public Visit(Customer customer, Date date, double serviceExpensive, double productExpensive) {
        this.customer = customer;
        this.date = date;
        this.serviceExpensive = serviceExpensive;
        this.productExpensive = productExpensive;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public double getServiceExpensive() {
        return serviceExpensive;
    }

    public void setServiceExpensive(double serviceExpensive) {
        this.serviceExpensive = serviceExpensive;
    }

    public double getProductExpensive() {
        return productExpensive;
    }

    public void setProductExpensive(double productExpensive) {
        this.productExpensive = productExpensive;
    }

    @Override
    public String toString() {
        return "Visit [customer=" + customer + ", date=" + date + ", productExpensive=" + productExpensive
                + ", serviceExpensive=" + serviceExpensive + "]";
    }

}
