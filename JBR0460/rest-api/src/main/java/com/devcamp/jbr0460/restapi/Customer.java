package com.devcamp.jbr0460.restapi;

public class Customer {
    String name;
    boolean member;
    String memberType;
    
    public Customer() {
    }

    public Customer(String name, boolean member, String memberType) {
        this.name = name;
        this.member = member;
        this.memberType = memberType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isMember() {
        return member;
    }

    public void setMember(boolean member) {
        this.member = member;
    }

    public String getMemberType() {
        return memberType;
    }

    public void setMemberType(String memberType) {
        this.memberType = memberType;
    }

    @Override
    public String toString() {
        return "Customer [member=" + member + ", memberType=" + memberType + ", name=" + name + "]";
    }

    
}
