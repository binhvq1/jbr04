package com.devcamp.jbr0470.restapi;

public class Cat extends Mammal {

    public Cat() {
    }

    public Cat(String name) {
        super(name);
    }

    public void greets() {
        System.out.println("Meow");
    }

    @Override
    public String toString() {
        return "Cat [Mammal [Animal = "+ name +"]]]";
    }
    
    
}
