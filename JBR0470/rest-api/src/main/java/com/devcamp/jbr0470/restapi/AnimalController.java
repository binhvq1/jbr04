package com.devcamp.jbr0470.restapi;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AnimalController {
    @CrossOrigin
    @GetMapping("/cats")
    public static String getCats() {
        ArrayList<Cat> listCats = new ArrayList<Cat>(); 
        Cat cat1 = new Cat("cat1");
        Cat cat2 = new Cat("cat2");
        Cat cat3 = new Cat("cat3");

        listCats.add(cat1);
        listCats.add(cat2);
        listCats.add(cat3);

        return String.format("Danh sách mèo con \n %s", listCats);
    }

    @GetMapping("/dogs")
    public static String getDogs() {
        ArrayList<Dog> listDogs = new ArrayList<Dog>(); 
        Dog dog1 = new Dog("Dog1");
        Dog dog2 = new Dog("Dog2");
        Dog dog3 = new Dog("Dog3");

        listDogs.add(dog1);
        listDogs.add(dog2);
        listDogs.add(dog3);

        return String.format("Danh sách chó con \n %s", listDogs);
    }
}
