package com.devcamp.jbr0470.restapi;

public class Mammal extends Animal {
    
    public Mammal() {
    }

    public Mammal(String name) {
        super(name);
    }

    @Override
    public String toString() {
        return "Mammal ["+ name +"]";
    }
    
}
