package com.devcamp.jbr041.restapi;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CircleCylinderController {
    @CrossOrigin
    @GetMapping("/circle-area")
    public String getCircleArea(@RequestParam("radius") double radius) {
        Circle circle = new Circle(radius);
        return String.format("Diện tích hình tròn = %s", circle.getArea());
    }


    @CrossOrigin
    @GetMapping("/cylinder-volume")
    public String getCylinderVolume(@RequestParam("radius") double radius,@RequestParam("height") double height) {
        Cylinder cylinder = new Cylinder(radius,height);
        return String.format("Thể tích hình trụ = %s", cylinder.getCylinderVolume());
    }
}
