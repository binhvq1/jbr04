package com.devcamp.jbr041.restapi;

public class Cylinder extends Circle {
    double height;

    public Cylinder(double height) {
        this.height = height;
    }

    public Cylinder(double radius, double height) {
        super(radius);
        this.height = height;
    }

    public Cylinder(double radius, String color, double height) {
        super(radius, color);
        this.height = height;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getCylinderVolume() {
        double getCylinderVolume = height * Math.pow(radius, 2) * Math.PI;
        return getCylinderVolume;
    }

    @Override
    public String toString() {
        return "Cylinder [height=" + height + "]";
    }
    
}
