package com.devcamp.jbr0450.restapi;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CustomerAccountController {
    @CrossOrigin
    @GetMapping("/accounts")
    public static String main(String[] args) {
        Customer customer1 = new Customer(1, "Nam", 10);
        Customer customer2 = new Customer(2, "Phuong", 20);
        Customer customer3 = new Customer(3, "Hung", 30);

        System.out.println(customer1.toString());
        System.out.println(customer2.toString());
        System.out.println(customer3.toString());

        Account account1 = new Account(123, customer1, 1.0);
        Account account2 = new Account(123, customer1, 1.0);
        Account account3 = new Account(123, customer1, 1.0);

        System.out.println(account1.toString());
        System.out.println(account2.toString());
        System.out.println(account3.toString());

        ArrayList<Account> listAccount = new ArrayList<Account>();

        listAccount.add(account1);
        listAccount.add(account2);
        listAccount.add(account3);

        return String.format("Danh sách accounts \n %s", listAccount);
    }
}
